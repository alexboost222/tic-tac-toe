﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace DefaultNamespace
{
    public class TicTacToeView : MonoBehaviour
    {
        [SerializeField] private TicTacToeElementView elementViewTemplate;

        public IObservable<int> ElementClicked => _elementClicked;

        private readonly Subject<int> _elementClicked = new();
        private readonly List<TicTacToeElementView> _elements = new();

        private TicTacToePresenter _ticTacToePresenter;

        private void Awake()
        {
            _elementClicked.AddTo(this);
            _ticTacToePresenter = new TicTacToePresenter(this).AddTo(this);
            BindModel(_ticTacToePresenter.Model);
        }

        private void BindModel(TicTacToeModel model)
        {
            for (int elementNumber = 0; elementNumber < model.Size * model.Size; elementNumber++)
            {
                TicTacToeElementView elementView = Instantiate(elementViewTemplate, elementViewTemplate.transform.parent);
                elementView.Clicked.Subscribe(OnElementClicked).AddTo(elementView);
                _elements.Add(elementView);
                elementView.gameObject.SetActive(true);
            }
            model.PlayingFieldChanged.Subscribe(OnPlayingFieldChanged).AddTo(this);
        }

        private void OnElementClicked(TicTacToeElementView elementView)
        {
            int elementIndex = _elements.IndexOf(elementView);
            _elementClicked.OnNext(elementIndex);
        }

        private void OnPlayingFieldChanged(ETicTacToeElementType[,] playingField)
        {
            for (var x = 0; x < playingField.GetLength(0); x++)
            {
                for (var y = 0; y < playingField.GetLength(1); y++)
                {
                    ETicTacToeElementType elementType = playingField[x, y];
                    _elements[x * playingField.GetLength(1) + y].SetType(elementType);
                }
            }
        }
    }
}