﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class TicTacToeElementView : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Image image;
        [SerializeField] private Sprite circleSprite;
        [SerializeField] private Sprite crossSprite;

        public IObservable<TicTacToeElementView> Clicked => _clicked;

        public void SetType(ETicTacToeElementType type)
        {
            image.sprite = type switch
            {
                ETicTacToeElementType.Empty => null,
                ETicTacToeElementType.O => circleSprite,
                ETicTacToeElementType.X => crossSprite,
                _ => throw new NotImplementedException()
            };
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                _clicked.OnNext(this);
            }
        }

        private readonly Subject<TicTacToeElementView> _clicked = new();

        private void Awake()
        {
            _clicked.AddTo(this);
        }
    }
}