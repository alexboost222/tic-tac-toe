﻿using Utils;
using UniRx;

namespace DefaultNamespace
{
    public class TicTacToePresenter : DisposableCollector
    {
        public readonly TicTacToeModel Model;

        public TicTacToePresenter(TicTacToeView view)
        {
            view.ElementClicked.Subscribe(OnElementClicked).AddTo(this);
            Model = new TicTacToeModel(3).AddTo(this);
        }

        private void OnElementClicked(int index)
        {
            int x = index / Model.Size;
            int y = index % Model.Size;
            Model.TrySet(x, y);
        }
    }
}