﻿using System;
using UniRx;
using Utils;

namespace DefaultNamespace
{
    public class TicTacToeModel : DisposableCollector
    {
        public TicTacToeModel(int size)
        {
            PlayingField = new ETicTacToeElementType[size, size];
            _playingFieldChanged.AddTo(this);
        }

        public int Size => PlayingField.GetLength(0);
        public ETicTacToeElementType[,] PlayingField { get; }
        public IObservable<ETicTacToeElementType[,]> PlayingFieldChanged => _playingFieldChanged;

        public bool TrySet(int x, int y)
        {
            if (x < 0 || x >= Size || y < 0 || y >= Size)
                return false;
            if (PlayingField[x, y] != ETicTacToeElementType.Empty)
                return false;
            if (IsGameEnded(out _))
                return false;
            PlayingField[x, y] = WhoseTurn();
            _playingFieldChanged.OnNext(PlayingField);
            return true;
        }

        public bool IsGameEnded(out ETicTacToeElementType winner)
        {
            winner = ETicTacToeElementType.Empty;
            bool winnerExists;
            
            // Check rows
            for (int x = 0; x < Size; x++)
            {
                int rowSum = 0;
                for (int y = 0; y < Size; y++)
                {
                    rowSum += (int)PlayingField[x, y];
                }

                winnerExists = TryDetermineWinner(rowSum, out winner);
                if (winnerExists)
                    return true;
            }
            
            // Check columns
            for (int y = 0; y < Size; y++)
            {
                int colSum = 0;
                for (int x = 0; x < Size; x++)
                {
                    colSum += (int)PlayingField[x, y];
                }

                winnerExists = TryDetermineWinner(colSum, out winner);
                if (winnerExists)
                    return true;
            }

            // Check main diagonal
            int diagSum = 0;
            for (int i = 0; i < Size; i++)
            {
                diagSum += (int)PlayingField[i, i];
            }
            
            winnerExists = TryDetermineWinner(diagSum, out winner);
            if (winnerExists)
                return true;

            // Check additional diagonal
            diagSum = 0;
            for (int i = 0; i < Size; i++)
            {
                diagSum += (int)PlayingField[i, Size - 1 - i];
            }
            
            winnerExists = TryDetermineWinner(diagSum, out winner);
            if (winnerExists)
                return true;

            // Check draw
            bool isDraw = true;
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    if (PlayingField[x, y] == ETicTacToeElementType.Empty)
                    {
                        isDraw = false;
                        break;
                    }
                }
            }

            return isDraw;
        }

        public ETicTacToeElementType WhoseTurn()
        {
            int sum = 0;
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    sum += (int)PlayingField[x, y];
                }
            }

            if (sum == -1)
                return ETicTacToeElementType.O;
            if (sum == 0)
                return ETicTacToeElementType.X;
            throw new Exception($"Couldn't determine whose turn. Invalid playing field state with sum {sum}.");
        }

        private readonly Subject<ETicTacToeElementType[,]> _playingFieldChanged = new();

        private static bool TryDetermineWinner(int sum, out ETicTacToeElementType winner)
        {
            winner = ETicTacToeElementType.Empty;
            if (sum == 3)
            {
                winner = ETicTacToeElementType.O;
                return true;
            }

            if (sum == -3)
            {
                winner = ETicTacToeElementType.X;
                return true;
            }

            return false;
        }
    }
}